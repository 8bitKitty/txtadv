#! /usr/bin/env python
# -*- coding: utf-8 -*-

import objects, creatures, exits
import os, json

debug = False
roomlib = []

class Room():
	def __init__(self, name, desc, objs, exits):
		self.name = name
		self.desc = desc
		self.objs = objs
		self.exits = exits

def room_decoder(room):
	rooms = []
	if debug: print json.dumps(room, indent=2, sort_keys=False)
	for k, v in room['Rooms'].iteritems():
		try:
			rooms.append(Room(room['Rooms'][k]['name'], room['Rooms'][k]['desc'], objects.parse_objs(room['Rooms'][k]['objs']), room['Rooms'][k]['exits']))
		except Exception as e:
			print e
	return rooms

roomlib = room_decoder(json.load(open(os.path.join('core/resources','rooms.json'),'r')))

def findroom(room):
	for r in roomlib:
		if r.name == room:
			return r

class Cave(Room):
	def __init__(self):
		self.name = "Eerie Cave"
		self.desc = "You find yourself in a cave with a flickering torch on the wall.  You can make out four paths, each equally as dark and foreboding."
		self.objs = objects.parse_objs(["Rock"])

class Coffee(Room):
	def __init__(self):
		self.name = "Cafe"
		self.desc = "You're inside the local coffeehouse.  The aroma of coffee wafts through the air, almost overwhelmingly so."
		self.objs = objects.parse_objs(["Cup of Coffee","Apple","Table","Chair"])
		self.objs = self.objs.append()
