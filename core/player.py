#! /usr/bin/env python

from objects import Obj
import objects, rooms
from colorama import Fore, Back, Style
from collections import Counter
from num2words import num2words

class Player(Obj):
	def __init__(self):
		self.name = 'Player'
		self.inventory = objects.parse_objs([["Orange",2],["Apple",3],["Bagel",1]])
		self.location = rooms.findroom("Cafe")

	def inv_print(self):
		if len(self.inventory) == 0:
			print "Carrying:\n (nothing)"
			return

		print "Carrying:"
		for item,x in dict(Counter(self.inventory)).iteritems():
			if x > 1:
				print "  %s %s" % (Style.BRIGHT+Fore.WHITE+num2words(x).capitalize(),item.plural+Style.RESET_ALL)
			else:
				print "  " + (Style.BRIGHT+Fore.WHITE+item.name+Style.RESET_ALL)