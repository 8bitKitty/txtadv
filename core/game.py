#! /usr/bin/env python
# -*- coding: utf-8 -*-

import colorama
from cmds import CmdPrompt
import cmds

def play():
	colorama.init()
	cmds.look_room(cmds.room)
	CmdPrompt().cmdloop()
	colorama.deinit()
