#! /usr/bin/env python

from objects import Obj

class Creature(Obj):
	def __init__(self, name, description):
		self.name = name
		self.description = description
		self.hp = hp

class Enemy(Creature):
	def __init__(self, damage):
		self.damage = damage

class GiantSpider(Enemy):
	def __init__(self):
		super().__init__(
			name = "Giant Spider",
			description = "A giant, black arachnid.  It doesn't look very friendly.",
			hp = 10,
			damage = 2
			)

class Rat(Enemy):
	def __init__(self):
		super().__init__(
			name = "Rat",
			description = "A mostly average rat, but it looks kind of vicious.  Best be careful.",
			hp = 3,
			damage = 1
			)
