#! /usr/bin/env python

import os, json
import pprint

debug = False
objlib = []

class Obj():
	def __init__(self, name, plural, type, keys, desc):
		self.name = name
		self.plural = plural
		self.type = type
		self.keys = keys
		self.desc = desc

class Consumable(Obj):
	def __init__(self, name, plural, type, keys, desc,):
		Obj.__init__(self, name, plural, type, keys, desc)

def object_decoder(obj):
	objects = []
	if debug: print json.dumps(obj, indent=2, sort_keys=False)
	for otype in obj:
		for otype2, keys in obj[otype].iteritems():
			for value in keys:
				try:
					objects.append(
						Obj(obj[otype][otype2][value]['name'],
							obj[otype][otype2][value]['plural'],
							otype2,
							obj[otype][otype2][value]['keys'],
							obj[otype][otype2][value]['desc'])
						)
				except Exception as e:
					print e
	return objects

objlib = object_decoder(json.load(open(os.path.join('core/resources','objects.json'),'r')))

def parse_objs(objs):
	new_objs = []
	for obj in objs:
		for e in objlib:
			if obj[0] == e.name:
				for _ in range(obj[1]):
					new_objs.append(e)
			elif obj == e.name:
				new_objs.append(e)
	if new_objs != []:
		return new_objs

def create(obj):
	new_object = None
	for e in objlib:
		if obj == e.name:
			new_object = e
	if new_object != None:
		return new_object
