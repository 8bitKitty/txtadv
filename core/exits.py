#! /usr/bin/env python

import objects, rooms
from objects import Obj

class Exit(Obj):
	def __init__(self, name, connect_to):
		self.name = name
		self.connect_to = connect_to
