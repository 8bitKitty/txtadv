#! /usr/bin/env python
# -*- coding: utf-8 -*-

import textwrap
import os, json, re
from num2words import num2words
from collections import Counter

import cmd
from colorama import Fore, Back, Style

import rooms, objects
from player import Player

linsep = "─"

player = Player()
room = player.location

def screen_wh(wh):
	screen_height, screen_width = os.popen('stty size', 'r').read().split()
	if wh == 'w':
		return int(screen_width)
	elif wh == 'h':
		return int(screen_height)
	else:
		return 80

def obj_format(word):
	if word[1] == 1:
		if word[0][0] in 'AEIOUaeiou':
			# return ['an',word]
			return "an %s" % Style.BRIGHT + Fore.YELLOW + word[0] + Style.RESET_ALL
		else:
			# return ['a',word] 
			return "a %s" % Style.BRIGHT + Fore.YELLOW + word[0] + Style.RESET_ALL
	else:
		return Style.BRIGHT + Fore.YELLOW + num2words(word[1]) + ' ' + word[0] + Style.RESET_ALL

def is_or_are(word):
	pass

def look_room(room):
	roomname_len = len(room.name)
	if roomname_len % 2 == 0: roomname_len += 2
	else: roomname_len += 1
	print(color('»' + linsep * (roomname_len) + "«", Fore.CYAN, sty=Style.BRIGHT))
	print(color(("~"+room.name+"~").center(roomname_len+2), Fore.CYAN, sty=Style.BRIGHT))
	print(color('»' + linsep * (roomname_len) + "«", Fore.CYAN, sty=Style.BRIGHT))
	for line in textwrap.wrap(room.desc, screen_wh('w')): print line
	temp_objlist = []
	temp_roomobjs = dict(Counter(room.objs))
	for obj,x in temp_roomobjs.iteritems():
		if x > 1: temp_objlist.append([str(obj.plural.lower()),x])
		elif x == 1: temp_objlist.append([str(obj.name.lower()),x])
	if len(temp_objlist) >= 2:
		text = ''
		temp_objlist = sorted(temp_objlist)
		verb = 'is'
		if sorted(temp_objlist)[0][1] > 1:
			verb = 'are'
		text = "There %s %s and %s here." % (verb, ', '.join(str(obj_format(x)) for x in temp_objlist[:-1]), obj_format(temp_objlist[-1]))
		print ''
		for line in textwrap.wrap(text, screen_wh('w')*1.5): print line
	elif len(temp_objlist) == 1:
		print ''
		if temp_objlist[0][1] > 1: print "There are %s here." % ("".join(obj_format(temp_objlist[0])))
		else: print "There is %s here." % ("".join(obj_format(temp_objlist[0])))
	exits = []
	for k,v in room.exits:
		for r in rooms.roomlib:
			if k == r.name and v.lower() in ['north','south','east','west','up','down']:
				exits.append("\n  [%s] %s" % (Fore.RED+v+Style.RESET_ALL,k))
	if exits != []: print color(Fore.CYAN + Style.BRIGHT + "\nExits: %s" % (Style.RESET_ALL + "".join(exits)))

# Could use some improvement.. player.location and room global should update simultaneously in the future..
def moveDirection(direction):
	global room
	new_room = []
	"""
	try:A helper function that changes the location of the player."""
	for k,v in room.exits:
		if direction == v.lower() or direction[0] == v[0].lower():
			new_room = k,v
			break
	if new_room != []:
		print('You travel %s.\n' % new_room[1].lower())
		player.location = rooms.findroom(new_room[0])
		room = rooms.findroom(new_room[0])
		look_room(player.location)
		return
	else:
		print('You cannot move in that direction.')
		return


def getAllDescWords(itemList):
	"""Returns a list of "description words" for each item named in itemList."""
	itemList = list(set(itemList)) # make itemList unique
	descWords = []
	for item in itemList:
		descWords.extend(item.keys)
	return list(set(descWords))

def getAllFirstDescWords(itemList):
	"""Returns a list of the first "description word" in the list of
	description words for each item named in itemList."""
	itemList = list(set(itemList)) # make itemList unique
	descWords = []
	for item in itemList:
		descWords.append(item.keys[0])
	return list(set(descWords))

def getFirstItemMatchingDesc(desc, itemList):
	itemList = list(set(itemList)) # make itemList unique
	for item in itemList:
		if desc in item.keys:
			return item
	return None

def getAllItemsMatchingDesc(desc, itemList):
	itemList = list(set(itemList)) # make itemList unique
	matchingItems = []
	for item in itemList:
		if desc in item.keys:
			matchingItems.append(item)
	return matchingItems

class CmdPrompt(cmd.Cmd):
	prompt = "\n> "

	def default(self, arg):
		print "I don't understand. Perhaps try the 'help' command?"

	def do_quit(self, arg):
		"""Quit the game."""
		return True

	def help_combat(self):
		print "Combat is not yet implemented."

	def do_north(self, arg):
		"""Go to the area to the north, if possible."""
		moveDirection('north')

	def do_south(self, arg):
		"""Go to the area to the south, if possible."""
		moveDirection('south')

	def do_east(self, arg):
		"""Go to the area to the east, if possible."""
		moveDirection('east')

	def do_west(self, arg):
		"""Go to the area to the west, if possible."""
		moveDirection('west')

	def do_up(self, arg):
		"""Go to the area upwards, if possible."""
		moveDirection('up')

	def do_down(self, arg):
		"""Go to the area downwards, if possible."""
		moveDirection('down')

	def do_get(self, arg):
		"""Get the specified object, if possible, and put it in the player's inventory."""
		try:
			num, getitem = arg.lower().split(' ')
		except:
			getitem = arg.lower()
			num = 1

		for item in getAllItemsMatchingDesc(getitem, room.objs):
			if item.type != 'Decorative':
				if num == 1:
					print "You get the %s." % (Style.BRIGHT+Fore.YELLOW+item.name.lower()+Style.RESET_ALL)
				else:
					print "You get %s %s." % (Style.BRIGHT+Fore.YELLOW+num2words(int(num)), item.plural.lower()+Style.RESET_ALL)
				for _ in range(int(num)):
					player.inventory.append(item)
					room.objs.remove(item)
				return
			elif item.type == 'Decorative':
				if num == 1:
					print "You can't get the %s." % (Style.BRIGHT+Fore.YELLOW+item.name.lower()+Style.RESET_ALL)
				else:
					print "You can't get %s %s." % (Style.BRIGHT+Fore.YELLOW+num2words(int(num)), item.plural.lower()+Style.RESET_ALL)
				return
		
		for item in getAllItemsMatchingDesc(getitem, player.inventory):
			print "You already have the %s." % (Style.BRIGHT+Fore.YELLOW+item.name.lower()+Style.RESET_ALL)
			return

		print "That's not here."

	def do_go(self, arg):
		moveDirection(arg[0])

	def do_create(self, arg):
		pass

	def do_drop(self, arg):
		"""Drop a specified object, if possible, from the player's inventory."""
		try:
			num, dropitem = arg.lower().split(' ')
		except:
			dropitem = arg.lower()
			num = 1

		for item in getAllItemsMatchingDesc(dropitem, player.inventory):
			if num == 1:
				print "You drop the %s." % (Style.BRIGHT+Fore.YELLOW+item.name.lower()+Style.RESET_ALL)
			else:
				print "You drop %s %s." % (Style.BRIGHT+Fore.YELLOW+num2words(int(num)), item.plural.lower()+Style.RESET_ALL)
			for _ in range(int(num)):
				room.objs.append(item)
				player.inventory.remove(item)
			return

		if dropitem not in player.inventory:
			print "You don't have that."

	def do_inventory(self, arg):
		"""List the contents of the player's inventory."""
		player.inv_print()

	def do_look(self, arg):
		"""Look at the specified object, direction, or area."""
		looking = arg.lower()
		if looking == "":
			look_room(room)
			return

		# see if the item being looked at is on the ground at this location
		item = getFirstItemMatchingDesc(looking, room.objs)
		if item != None:
			print('\n'.join(textwrap.wrap(item.desc, screen_wh('w'))))
			return

		# see if the item being looked at is in the inventory
		item = getFirstItemMatchingDesc(looking, player.inventory)
		if item != None:
			print('\n'.join(textwrap.wrap(item.desc, screen_wh('w'))))
			return

		print('You do not see that nearby.')

	def complete_look(self, text, line, begidx, endidx):
		possibleItems = []
		looking = text.lower()

		# get a list of all "description words" for each item in the inventory
		invDescWords = getAllDescWords(player.inventory)
		groundDescWords = getAllDescWords(room.objs)

		for descWord in groundDescWords:
			if line.startswith('look %s' % (descWord)):
				return []

		# if the user has only typed "look" but no item name, show all items on ground, shop and directions:
		if looking == '':
			possibleItems.extend(getAllFirstDescWords(room.objs))
			possibleItems.extend(getAllFirstDescWords(player.inventory))
			return list(set(possibleItems))

		# otherwise, get a list of all "description words" for ground items matching the command text so far:
		for descWord in groundDescWords:
			if descWord.startswith(looking):
				possibleItems.append(descWord)

		for descWord in invDescWords:
			if descWord.startswith(looking):
				possibleItems.append(descWord)

		return list(set(possibleItems))

	def do_say(self, arg):
		"""Say something out loud to the entire room."""
		if not arg == '':
			if arg.endswith('!'):
				print 'You yell, "%s"' % arg
			elif arg.endswith('?'):
				print 'You ask, "%s"' % arg
			else:
				print 'You say, "%s"' % arg
		else:
			print "I didn't qute get that."

	def do_eat(self, arg):
		"""Consume the specified object, if it's consumable."""
		eating = arg.lower()
		if eating == "":
			print "Eat what?"
			return

		# see if the item being eaten is in the inventory
		item = getFirstItemMatchingDesc(eating, player.inventory)
		if item != None:
			if item.type == "Food":
				print('You eat your %s.' % (Style.BRIGHT+Fore.YELLOW+item.name.lower()+Style.RESET_ALL))
				player.inventory.remove(item)
				return
			if item.type == "Drink":
				print('You drink your %s and now have an %s.' % ((Style.BRIGHT+Fore.YELLOW+item.name.lower()+Style.RESET_ALL),(Style.BRIGHT+Fore.YELLOW+'empty cup'+Style.RESET_ALL)))
				player.inventory.append(objects.create("Empty Cup"))
				player.inventory.remove(item)
				return
			else:
				print("You can't consume your %s." % (Style.BRIGHT+Fore.YELLOW+item.name.lower()+Style.RESET_ALL))
				return

		# see if the item being eaten is on the ground at this location
		item = getFirstItemMatchingDesc(eating, room.objs)
		if item != None:
			if item.type == "Food":
				print('You eat the %s.' % (Style.BRIGHT+Fore.YELLOW+item.name.lower()+Style.RESET_ALL))
				room.objs.remove(item)
				return
			if item.type == "Drink":
				print('You drink the %s and leave behind an %s.' % ((Style.BRIGHT+Fore.YELLOW+item.name.lower()+Style.RESET_ALL),(Style.BRIGHT+Fore.YELLOW+'empty cup'+Style.RESET_ALL)))
				room.objs.append(objects.create("Empty Cup"))
				room.objs.remove(item)
				return
			else:
				print("You can't consume the %s." % (Style.BRIGHT+Fore.YELLOW+item.name.lower()+Style.RESET_ALL))
				return

		else:
			print('You do not see that nearby.')

	# def do_debug(self, arg):
	# 	if arg:
	# 		if arg == 'rooms':
	# 			print json.dumps(rooms.roomlib, default=lambda o: o.__dict__, indent=4, sort_keys=False)
	# 		elif arg == 'objects':
	# 			print json.dumps(objects.objlib, default=lambda o: o.__dict__, indent=4, sort_keys=False)
	# 		else:
	# 			print "You must specify 'rooms' or 'objects' as the debug argument."
	# 	else:
	# 		print "You must specify 'rooms' or 'objects' as the debug argument."

	#inventory shortcuts
	do_l = do_look
	do_take = do_get
	do_drink = do_eat
	do_inv = do_inventory
	do_i = do_inventory
	do_q = do_quit
	complete_l = complete_look
	complete_drop = complete_look
	complete_get = complete_look
	complete_take = complete_look
	complete_eat = complete_look
	complete_drink = complete_look

	#movement shortcuts
	do_n = do_north
	do_s = do_south
	do_e = do_east
	do_w = do_west
	do_u = do_up
	do_d = do_down

def color(text, fcol=Fore.RESET, bcol=Back.RESET, sty=Style.NORMAL):
	return fcol + bcol + sty + text + Style.RESET_ALL
